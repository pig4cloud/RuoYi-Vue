package com.ruoyi.framework.security.handle;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author lengleng
 * @date 2020/4/4
 *
 * SSO 方式直接返回true
 */
public class SsoBCryptPasswordEncoder extends BCryptPasswordEncoder {

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {

        // 判断原文是否为 SSO,此处建议从TTL 获取判断标识，避免超级密码的问题
        if ("SSO".equals(rawPassword.toString())) {
            return true;
        }


        return super.matches(rawPassword, encodedPassword);
    }
}
